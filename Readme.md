## A Challenge

Write a program that prints all the numbers from 1 to 100. However, for multiples of 3, instead of the number, print "Foo". For multiples of 5 print "Bar". For numbers which are multiples of both 3 and 5, print "FooBar".

But here's the catch: you can use only one if. No multiple branches, ternary operators or else.

Requirements:
1 if
You can't use else, else if or ternary
Unit tests

---
Instructions:

You need node.js and yarn installed.

From command line ...
- Use ```yarn install``` to install challenge dependencies
- Use ```yarn test``` to run challenge tests
- Use ```yarn start``` to run challenge and print output to console
