'use strict'

const numbers = [...Array(100).keys()]

function isMultipleOf3AndOr5(n) {
  const isOf3 = (xIsMultipleOfY(n, 3) && 'Foo') || n
  const isOf5 = (xIsMultipleOfY(n, 5) && 'Bar') || n

  function isOf3And5() {
    return isOf3 && isOf5 && isOf3 + isOf5
  }

  function isOf5ButNotOf3() {
    return isOf5 && !isNaN(isOf3) && isOf5
  }

  function isOf3ButNotOf5() {
    return isOf3 && !isNaN(isOf5) && isOf3
  }

  return isOf3ButNotOf5() || isOf5ButNotOf3() || isOf3And5()
}

function xIsMultipleOfY(x, y) {
  return x % y == 0
}

module.exports = {
  numbers,
  xIsMultipleOfY,
  isMultipleOf3AndOr5
}
