'use strict'

const { isMultipleOf3AndOr5, numbers } = require('./core')

function run() {
  const result = numbers.map(i => {
    return isMultipleOf3AndOr5(++i)
  })

  result.forEach(r => console.log(r))
}

run()
