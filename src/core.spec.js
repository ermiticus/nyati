'use strict'

const { isMultipleOf3AndOr5, numbers, xIsMultipleOfY } = require('./core')

describe('Foo Bar multiples 1 to 100: ', () => {
  test('numbers length is 100', () => {
    expect(numbers.length).toBe(100)
  })

  describe('xIsMultipleOfY: ', () => {
    test('it returns false for non multiples', () => {
      expect(xIsMultipleOfY(1, 3)).toBe(false)
      expect(xIsMultipleOfY(1, 5)).toBe(false)
      expect(xIsMultipleOfY(3, 5)).toBe(false)
      expect(xIsMultipleOfY(5, 3)).toBe(false)
    })

    test('it returns true for multiples', () => {
      expect(xIsMultipleOfY(3, 3)).toBe(true)
      expect(xIsMultipleOfY(5, 5)).toBe(true)
      expect(xIsMultipleOfY(15, 3)).toBe(true)
      expect(xIsMultipleOfY(15, 5)).toBe(true)
    })
  })

  describe('isMultipleOf3AndOr5', () => {
    test('it returns the tested number when is not a multiple of 3 or 5', () => {
      expect(isMultipleOf3AndOr5(1)).toBe(1)
      expect(isMultipleOf3AndOr5(2)).toBe(2)
      expect(isMultipleOf3AndOr5(4)).toBe(4)
      expect(isMultipleOf3AndOr5(7)).toBe(7)
      expect(isMultipleOf3AndOr5(8)).toBe(8)
    })

    test('it returns Foo for multiples of only 3', () => {
      expect(isMultipleOf3AndOr5(3)).toEqual('Foo')
      expect(isMultipleOf3AndOr5(9)).toEqual('Foo')
    })

    test('it returns Bar for multiples of only 5', () => {
      expect(isMultipleOf3AndOr5(5)).toEqual('Bar')
      expect(isMultipleOf3AndOr5(10)).toEqual('Bar')
    })

    test('it returns FooBar for multiples of 3 AND 5', () => {
      expect(isMultipleOf3AndOr5(15)).toEqual('FooBar')
      expect(isMultipleOf3AndOr5(30)).toEqual('FooBar')
      expect(isMultipleOf3AndOr5(60)).toEqual('FooBar')
    })
  })
})
